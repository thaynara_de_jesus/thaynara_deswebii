# Notas de Aula #

Anotações de aula, dicas, links, etc.

## Links importantes ##

* [How to Tango with Django](http://www.tangowithdjango.com/)
* [Django](https://www.djangoproject.com/)


## Configurações do ambiente:

### Instalação de git, pip, virtualenv e virtualenvwrapper

* `sudo apt-get install git python-pip virtualenv virtualenvwrapper`

### Verificação das versões do python e django:

* `python --version`
* `python -c "import django; print(django.get_version())`

### Configuração inicial do git na máquina:
* `git config --global user.name "John Doe"`
* `git config --global user.email johndoe@example.com`
* `git config --global core.editor nano`
* `git config --global credential.helper 'cache --timeout=3600'`

### Instalação do Pycharm:
* copiar o arquivo pycharm-3.4.1.tgz para a pasta /opt/:
`sudo cp /Downloads/pycharm-3.4.1.tgz /opt`
* descompactar o arquivo:
`sudo tar -xzvf pycharm-3.4.1.tgz`
* copiar o arquivo de ícone para a pasta correta:
`sudo cp /opt/pycharm-3.4.1/pycharm.desktop /usr/share/applications/`
* criar um link simbólico para a pasta do pycharm:
`sudo ln -s /opt/pycharm-3.4.1 /opt/pycharm`
* Licença - usuário:
`Instituto Federal Catarinense`
* Licença chave:
`488075-30052014
00000"zcyIZ!vklKZe4mnewqb7Pd3S
ngn90tKswdqvlANfT0"QpxcAMmOrNG
1cF!y"lzagjJ9Vfh3ZuBLeRUzn5a0q`

#Criar repositório#

* `mkdir local_da_pasta`
* `cd local_da_pasta`
* `git init`
* `git remote add origin https://thaynara_de_jesus@bitbucket.org/thaynara_de_jesus/thaynara_deswebii.git`

* `echo "Thaynara de Jesus" >> contributors.txt`
* `git add contributors.txt`
* `git commit -m "Initial commit with contributors"`
* `git push -u origin master`

##pegar tudo que esta na nuvem##

* `git pull`

#Instalar pip#

* `sudo apt-get install python-pip python3-pip` (instalar todos os pacotes que são relacionados ao python)




#Instalar virtualenv#

* `pip install virtualenv virtualenvwrapper`

* `echo "source virtualenvwrapper.sh" >> ~/.bashrc"` (escreve no final do arquivo bashrc)


#Criar manualmente máquina virtual#
* `virtualenv bsi6`
* `mkdir .virtualenvs`
* `cd .virtualenvs/`
* `virtualenv rango`
* `cd rango`
* `cd bin`
* `source ~/.virtualenvs/rango/bin/activate`  (é o que faz o "workon")
Entrar na pasta do projeto
* `cd thaynara_deswebii`
* `pip install -r requirements.txt` 

#Criando a márquina virtual#

* `mkvirtualenv nome_projeto`

* `deactivate (desativar)`
* `workon nome_projeto (ativar)`

* `pip list`
* `pip freeze`


#Instalar Django#

* `pip install -U django==1.7`


#Instalar pillow#

* `pip install pillow` (pacote que faz gerenciamento de imagem)


#Grava os programas no arquivo#

* `pip freeze > requirements.txt`


#Instala os programas se não tem#

* `pip install -r requirements.txt`


#Verificar a versão python#

* `python --version`


#Versão Django#

* `python -c "import django; print(django.get_version())"`


#Iniciar projeto#

* 'django-admin.py startproject tango_with_django_project'


#Criar projeto#

* 'django-admin.py startproject tango_with_django_project'


gitignora >>>>>>>>>> ignora pacotes


#Colocar python no ar#

* 'python manage.py runserver'


#Cria aplicação#

* 'python manage.py startapp rango'


##senha root do lab: 4 d 1 min##


#Baixar pycharm pacotes#

##no terminal entrar com sudo "su"##

##mover o pacote compactado que esta em download para o apt##

* 'sudo mv home/aluno/Downloads/nomedopacote'

##para decompactar##

* 'tar -xzvf pucharm-profissional-4.5.3.tar.gz'

#Acessar o shell#

* '$ python manage.py shell '

# Import the Category model from the Rango application
>>> from rango.models import Category

# Show all the current categories
>>> print Category.objects.all()
[] # Returns an empty list (no categories have been defined!)

# Create a new category object, and save it to the database.
>>> c = Category(name="Test")
>>> c.save()

# Now list all the category objects stored once more.
>>> print Category.objects.all()
[<Category: test>] # We now have a category called 'test' saved in the database!

# Quit the Django shell.
>>> quit()


# Criar banco
* 'workon rango_bsi6'
* 'python manage.py migrate'
* 'python manage.py createsuperuser'
* 'python populate_rango.py'


Abrir console Bash do PythonAnywhere Consoles
Digitar comandos:

$ Source virtualenvwrapper.sh 
$ mkvirtualenv rango

O primeiro importa o ambiente virtual.
O segundo cria um novo ambiente virtual chamado rango.

$ ls -la

exibe os diretórios, dentre esses esta o .virtualenvs
Este é o diretório no qual todos os ambientes virtuais e pacotes associados serão armazenados.

Para confirmar a instalação executar o comando:
$ which pip

Local em que o ativo pip binário está localizado:
/home/thaynara/.virtualenvs/rango/bin/pip

caminho do virtualenv
/home/thaynara/.virtualenvs/rango

Para saber o caminho da pasta que esta:
$ pwd


Local do meu projeto:
/home/thaynara/thaynara_deswebii/tango_with_django_project

## Para exercícios

### 1º cria nova url: porta de entrada. aponta para onde vou direcionar (aponta para a view)
	'url(r'^suggest_category/$', views.suggest_category, name='suggest_category'),'
### 2º cria views: apartir dela que o django será construido
	'def suggest_category(request):	return render(request, 'rango/cats.html',{})'
