#coding: utf-8

from django.shortcuts import render, redirect
from rango.models import User, UserProfile

def user_profile(request, username):

    context_dict={}
    u = None
    try:
        u = User.objects.get(username = username)
    except:
        return redirect('/rango/users_list/')

    try:
        user_profile = UserProfile.objects.get(user = u)
    except:
        user_profile = None

    context_dict['u'] = u
    context_dict['user_profile'] = user_profile
    return render(request, 'rango/user_profile.html', context_dict)