#coding: utf-8
from django.shortcuts import render
from rango.models import *
from django.views.generic import *


class About(View):
    template_name = 'rango/about.html'

    def get(self, request):
        context_dict = {}
        if request.session.get('visits'):
            visits = request.session.get('visits')
        else:
            visits = 0

        context_dict['visits'] = visits
        return render(request, self.template_name, context_dict)