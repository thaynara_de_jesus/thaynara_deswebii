#coding: utf-8

from django.shortcuts import render, redirect
from rango.models import User

def users_list(request):

    users = User.objects.all()
    print(users)
    context_dict = {}
    context_dict['users'] = users
    return render(request, 'rango/users_list.html', context_dict)